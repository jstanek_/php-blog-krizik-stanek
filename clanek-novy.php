<?php require_once('head.php');?>
<?php require_once('fce/check_permission.php');?>

<body>
<header>
    <img class="logoa" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
    <?php include('menu-admin2.php');?>
</header>

<div class="content">

    <h2>Založení nového článku</h2>



    <?php
        if (isset($_POST['submit'])) {

            $titulek = $_POST['title'];
            $kategorie = $_POST['kategorie'];
            $obsah = $_POST['obsah'];
            $iduser = $_SESSION['iduser'];

            $stmt = $conn->prepare("INSERT INTO clanky (titulek,obsah,idkategorie,idautor) VALUES ('".$titulek."','".$obsah."','".$kategorie."','".$iduser."')");
            $stmt->execute();
        } else {
    ?>

    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        <input type="text" name="title" placeholder="Titulek nového článku">
        <select name="kategorie">
            <?php
            $stmt = $conn->prepare("SELECT idkategorie, nazev FROM kategorie");
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                echo '<option value="'.$row['idkategorie'].'">'.$row['nazev'].'</option>';
            }
            ?>
        </select>
        <textarea name="obsah" rows="15" cols="80"></textarea>
        <input type="submit" name="submit"><input type="reset">
    </form>

    <?php
        }
    ?>




</div>
<?php include('footer.php');?>

</body>
</html>
