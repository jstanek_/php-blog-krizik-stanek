<?php require_once('head.php');?>
<body>
<header>
    <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
    <?php include('menu.php');?>
</header>

<div class="content">

    <?php

    session_destroy();
    header("Location: http://localhost/www/php-blog-krizik-stanek-master/index.php");

    ?>

</div>
<?php include('footer.php');?>

</body>
</html>
